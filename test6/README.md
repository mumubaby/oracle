﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。
  
## 实验步骤

### 确定制定的表

- 画出销售系统关系模式![1](p1.png)
- 确定表

| 表索引|表明|说明|
|:-----|:-----|:-----|
|表一|Product|存储公司所有产品目录|
|表二|product_type|产品类类别：对表一的产品进行分类，并以其进行约束（字典库）|
|表三|client|客户表：一般指企业（即法人）|
|表四|Orders|订单：即将合同细化的产品订单|

- 以下是各数据表的具体结构及约束等说明：
  
表一:Product(商品)
| 字段|类型|约束|说明|
|:-----|:-----|:-----|:-----|
|product_id|char(10)|主键|
|product_name|char(20)|非空|
|product_type|tinyint|外键||
|product_price|number|无|
|product_number|int|非空|
表二:Sale(销售记录表)
| 字段|类型|约束|说明|
|:-----|:-----|:-----|:-----|
|sale_id|tinyint|标识列,主键|
|sale_data|datetime|非空|
|client_id|char(8)|非空|
|product_id|cahr(10)|非空|
|sale_number|int|非空|
表三:client(客户)
| 字段|类型|约束|说明|
|:-----|:-----|:-----|:-----|
|client_id|Char(8)|主键|
|client_name|Char(30)|非空|
|client_tel|Char(15)|非空|
|client_ad|Varchar(200)|非空|
|idcard|char(20)|非空|
|client_order|Char(10)|外键|引用表四product_id|
表四:Order(订单)
| 字段|类型|约束|说明|
|:-----|:-----|:-----|:-----|
|product_id|Char(10)|外键|
|order_id|char(10)|主键|
|client_id|char(8)|外键|
|buy_number|int|非空正数|
|sum_money|int|非空正数|
|pay_status|char(3)|非空|
|delivery_status|char(3)|非空|

### 在Oracle创建数据库创建表

- 创建分区
  create tablespace DATA_TBS
  2  datafile '/home/oracle/app/oracle/oradata/datatbs.dbf'
  3  size 1G
  4  autoextend on
  5  next 10M
  6  /
  create tablespace INDEX_TBS
  2  datafile '/home/oracle/app/oracle/oradata/indextbs.dbf'
  3  size 1G
  4  autoextend on
  5  next 10M
  6  /

表空间已创建。

- 创建用户并授权
   CREATE USER sales_user IDENTIFIED BY password DEFAULT TABLESPACE data_tbs QUOTA UNLIMITED ON data_tbs QUOTA UNLIMITED ON index_tbs;

  用户已创建。

  SQL> GRANT CONNECT TO sales_user;

  授权成功。

  SQL> GRANT RESOURCE TO sales_user;
  SQL> CREATE USER admin_user IDENTIFIED BY password DEFAULT TABLESPACE data_tbs QUOTA UNLIMITED ON data_tbs QUOTA UNLIMITED ON index_tbs;

  用户已创建。

  SQL> GRANT CONNECT TO admin_user;

  授权成功。

  SQL> GRANT RESOURCE TO admin_user;

  授权成功。

  SQL> GRANT ALL PRIVILEGES TO admin_user;

  授权成功。

- 创建表设置约束条件,设置时间分区
- 创建包,并在里面写入存储过程
  CREATE OR REPLACE PACKAGE insrt_client AS
  PROCEDURE insert_data(p_num IN NUMBER, p_table_name IN VARCHAR2);
  END insrt_client;
  /

  CREATE OR REPLACE PACKAGE BODY insrt_client AS
    PROCEDURE insert_data(p_num IN NUMBER,  p_table_name IN VARCHAR2) IS
    v_id VARCHAR2(10);
    v_name VARCHAR2(50);
    v_phone VARCHAR2(20);
    v_idcard VARCHAR2(20);
    v_address VARCHAR2(100);
  BEGIN
      -- 生成id号
      FOR i IN 1..p_num LOOP
      v_id := 'ID' || TO_CHAR(i, 'FM00000');

      -- 生成随机名字
      SELECT name INTO v_name FROM (
        SELECT '张三' AS name FROM DUAL
        UNION ALL SELECT '李四' AS name FROM DUAL
        UNION ALL SELECT '王五' AS name FROM DUAL
        UNION ALL SELECT '赵六' AS name FROM DUAL
      ) ORDER BY DBMS_RANDOM.VALUE() FETCH FIRST 1 ROW ONLY;
      
      -- 生成随机电话号码
      v_phone := '13' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(100000000, 999999999)), 'FM000000000');
      
      -- 生成随机身份证号
      v_idcard := '3201' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(100000000, 999999999)), 'FM000000') || '01' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(1000, 9999)), 'FM0000');
      
      -- 生成随机地址
      SELECT address INTO v_address FROM (
        SELECT '北京市海淀区' AS address FROM DUAL
        UNION ALL SELECT '上海市浦东新区' AS address FROM DUAL
        UNION ALL SELECT '广州市天河区' AS address FROM DUAL
        UNION ALL SELECT '深圳市福田区' AS address FROM DUAL
      ) ORDER BY DBMS_RANDOM.VALUE() FETCH FIRST 1 ROW ONLY;
      
      -- 插入数据
      EXECUTE IMMEDIATE 'INSERT INTO ' || p_table_name || ' VALUES (:1, :2, :3, :4, :5)' USING v_id, v_name, v_phone, v_address ,v_idcard;
    END LOOP;

    COMMIT;
  END insert_data;
END insrt_client;
/
delete from customer;
BEGIN
  insrt_client.insert_data(30000, 'customer');
END;

- 插入数据
- RMAN备份
  
  ![备份1](备份1.png)![备份2](备份2.png)![备份3](备份3.png)![备份4](备份4.png)
