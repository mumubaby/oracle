
  CREATE TABLE "ADMIN_USER"."CUSTOMER" 
   (	"CLIENT_ID" VARCHAR2(10 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"CLIENT_NAME" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"CLIENT_TEL" VARCHAR2(15 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"CLIENT_AD" VARCHAR2(200 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"ID_CARD" VARCHAR2(20 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	 CONSTRAINT "CLIENT_ID" PRIMARY KEY ("CLIENT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS" ;


  CREATE TABLE "ADMIN_USER"."ORDERS" 
   (	"ORDER_ID" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"PRODUCT_ID" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"CLIENT_ID" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"BUY_NUMBER" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	"PAY_STATUS" VARCHAR2(20 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"DELIVERY_STATUS" VARCHAR2(20 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	 CONSTRAINT "ORDER_ID" PRIMARY KEY ("ORDER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS"  ENABLE, 
	 CONSTRAINT "PRODUCT_IDO" FOREIGN KEY ("PRODUCT_ID")
	  REFERENCES "ADMIN_USER"."PRODUCT" ("PRODUCT_ID") ENABLE, 
	 CONSTRAINT "CLIENT_IDO" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "ADMIN_USER"."CUSTOMER" ("CLIENT_ID") ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS" ;


  GRANT INSERT ON "ADMIN_USER"."ORDERS" TO "SALES_USER";
  GRANT DELETE ON "ADMIN_USER"."ORDERS" TO "SALES_USER";
  GRANT SELECT ON "ADMIN_USER"."ORDERS" TO "SALES_USER";
  GRANT UPDATE ON "ADMIN_USER"."ORDERS" TO "SALES_USER";



  CREATE TABLE "ADMIN_USER"."PRODUCT" 
   (	"PRODUCT_ID" VARCHAR2(10 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"PRODUCT_NAME" VARCHAR2(20 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"PRODUCT_PRICE" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	"PRODUCT_NUMBER" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	 CONSTRAINT "PRODUCT_ID" PRIMARY KEY ("PRODUCT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS" ;


  GRANT DELETE ON "ADMIN_USER"."PRODUCT" TO "SALES_USER";
  GRANT INSERT ON "ADMIN_USER"."PRODUCT" TO "SALES_USER";
  GRANT SELECT ON "ADMIN_USER"."PRODUCT" TO "SALES_USER";
  GRANT UPDATE ON "ADMIN_USER"."PRODUCT" TO "SALES_USER";




  CREATE TABLE "ADMIN_USER"."SALE" 
   (	"SALE_ID" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"SALE_DATE" DATE NOT NULL ENABLE, 
	"CLIENT_ID" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"PRODUCT_ID" VARCHAR2(30 BYTE) DEFAULT 0 NOT NULL ENABLE, 
	"SALE_NUMBER" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	 CONSTRAINT "SALE_ID" PRIMARY KEY ("SALE_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS"  ENABLE, 
	 CONSTRAINT "PRODUCT_IDS" FOREIGN KEY ("PRODUCT_ID")
	  REFERENCES "ADMIN_USER"."PRODUCT" ("PRODUCT_ID") ENABLE, 
	 CONSTRAINT "CLIENT_IDS" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "ADMIN_USER"."CUSTOMER" ("CLIENT_ID") ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS  LOGGING 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA_TBS" 
  PARTITION BY RANGE ("SALE_DATE") 
 (PARTITION "SALE_2020"  VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBSP_1" , 
 PARTITION "SALE_2021"  VALUES LESS THAN (TO_DATE(' 2022-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(INITIAL 8388608 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBSP_2" ) ;


  GRANT SELECT ON "ADMIN_USER"."SALE" TO "SALES_USER";
  GRANT UPDATE ON "ADMIN_USER"."SALE" TO "SALES_USER";
  GRANT DELETE ON "ADMIN_USER"."SALE" TO "SALES_USER";
  GRANT INSERT ON "ADMIN_USER"."SALE" TO "SALES_USER";
  
  create or replace PACKAGE find_info AS
  FUNCTION find_info_by_id_and_table(p_id IN varchar, p_table_name IN VARCHAR2) RETURN VARCHAR2;
END find_info;
create or replace PACKAGE BODY find_info AS
  FUNCTION find_info_by_id_and_table(p_id IN varchar, p_table_name IN VARCHAR2) RETURN VARCHAR2 IS
    v_info VARCHAR2(200);
  BEGIN
    IF p_table_name = 'sale' THEN
      SELECT s.sale_date || ',' || c.client_name || ',' || p.product_name || ',' || s.sale_number
      INTO v_info
      FROM sale s, customer c, product p
      WHERE s.sale_id = p_id
        AND s.client_id = c.client_id
        AND s.product_id = p.product_id;
    ELSIF p_table_name = 'customer' THEN
      SELECT client_name || ',' || client_ad|| ',' || client_tel|| ',' ||id_card
      INTO v_info
      FROM customer
      WHERE client_id = p_id;
 ELSIF p_table_name = 'orders' THEN
      SELECT product_id || ',' || client_id || ',' || buy_number|| ',' ||pay_status|| ',' ||delivery_status
      INTO v_info
      FROM orders
      WHERE order_id = p_id;

    ELSIF p_table_name = 'product' THEN
      SELECT product_name || ',' || product_price || ',' || product_number
      INTO v_info
      FROM product
      WHERE product_id = p_id;
    ELSE
      v_info := 'Invalid table name.';
    END IF;
    RETURN v_info;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 'Info not found.';
  END find_info_by_id_and_table;
END find_info;


create or replace PACKAGE BODY find_order AS
  PROCEDURE find_order_by_id(p_order_id IN NUMBER) IS
    
    v_client_id VARCHAR2(30);
    v_product_id VARCHAR2(30);
    v_sale_number NUMBER;
    v_pay_status VARCHAR2(30);
    v_delivery_status VARCHAR2(30);
  BEGIN
    SELECT client_id, product_id, buy_number, pay_status,delivery_status
    INTO v_client_id, v_product_id, v_sale_number, v_pay_status,v_delivery_status
    FROM orders
    WHERE order_id = p_order_id;
    DBMS_OUTPUT.PUT_LINE('Client ID: ' || v_client_id);
    DBMS_OUTPUT.PUT_LINE('Product ID: ' || v_product_id);
    DBMS_OUTPUT.PUT_LINE('Sale Number: ' || v_sale_number);
    DBMS_OUTPUT.PUT_LINE('Pay Status: ' || v_pay_status);
    DBMS_OUTPUT.PUT_LINE('Delivery Status: ' || v_delivery_status);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('Order not found.');
  END find_order_by_id;
END find_order;


create or replace PACKAGE BODY insrt_client AS
  PROCEDURE insert_data(p_num IN NUMBER, p_table_name IN VARCHAR2) IS
    v_id VARCHAR2(10);
    v_name VARCHAR2(50);
    v_phone VARCHAR2(20);
    v_idcard VARCHAR2(20);
    v_address VARCHAR2(100);
  BEGIN
    -- 生成id号
    FOR i IN 1..p_num LOOP
      v_id := 'ID' || TO_CHAR(i, 'FM00000');

      -- 生成随机名字
      SELECT name INTO v_name FROM (
        SELECT '张三' AS name FROM DUAL
        UNION ALL SELECT '李四' AS name FROM DUAL
        UNION ALL SELECT '王五' AS name FROM DUAL
        UNION ALL SELECT '赵六' AS name FROM DUAL
      ) ORDER BY DBMS_RANDOM.VALUE() FETCH FIRST 1 ROW ONLY;

      -- 生成随机电话号码
      v_phone := '13' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(100000000, 999999999)), 'FM000000000');

      -- 生成随机身份证号
      v_idcard := '3201' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(100000000, 999999999)), 'FM000000') || '01' || TO_CHAR(ROUND(DBMS_RANDOM.VALUE(1000, 9999)), 'FM0000');

      -- 生成随机地址
      SELECT address INTO v_address FROM (
        SELECT '北京市海淀区' AS address FROM DUAL
        UNION ALL SELECT '上海市浦东新区' AS address FROM DUAL
        UNION ALL SELECT '广州市天河区' AS address FROM DUAL
        UNION ALL SELECT '深圳市福田区' AS address FROM DUAL
      ) ORDER BY DBMS_RANDOM.VALUE() FETCH FIRST 1 ROW ONLY;

      -- 插入数据
      EXECUTE IMMEDIATE 'INSERT INTO ' || p_table_name || ' VALUES (:1, :2, :3, :4, :5)' USING v_id, v_name, v_phone, v_address ,v_idcard;
    END LOOP;

    COMMIT;
  END insert_data;
END insrt_client;
call sale_package.INSERT_CLIRNT(8000);
SELECT
  'SALE' || LPAD(level, 5, '0'),
  DATE '2022-01-01' + TRUNC(DBMS_RANDOM.VALUE(1, 365)),
  'USER' || LPAD(ROUND(DBMS_RANDOM.VALUE(1, 100)), 5, '0'),
  'PRODUCT' || LPAD(ROUND(DBMS_RANDOM.VALUE(1, 100)), 5, '0'),
  ROUND(DBMS_RANDOM.VALUE(1, 10))
FROM dual
CONNECT BY level <= 100000;


