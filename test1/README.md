### 班级：软件工程1班     学号：202010414108     姓名：雷冰端

---

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

数据库是pdborcl，用户是sys和hr

## 实验内容

* 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
* 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。



## 实验步骤

### 一、权限分配
![](D:\oracle\oracle text\oracle\text1\images\01.jpg)

![](D:\oracle\oracle text\oracle\text1\images\02.jpg)


### 二、查询测试

#### 查询1：
![](D:\oracle\oracle text\oracle\text1\images\03.png)

![](D:\oracle\oracle text\oracle\text1\images\04.png)



![](D:\oracle\oracle text\oracle\text1\images\05.png)



#### 查询2：

![](D:\oracle\oracle text\oracle\text1\images\06.png)

![](D:\oracle\oracle text\oracle\text1\images\07.png)



